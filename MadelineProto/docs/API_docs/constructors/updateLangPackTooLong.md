---
title: updateLangPackTooLong
description: updateLangPackTooLong attributes, type and example
---
## Constructor: updateLangPackTooLong  
[Back to constructors index](index.md)






### Type: [Update](../types/Update.md)


### Example:

```
$updateLangPackTooLong = ['_' => 'updateLangPackTooLong', ];
```  

Or, if you're into Lua:  


```
updateLangPackTooLong={_='updateLangPackTooLong', }

```


