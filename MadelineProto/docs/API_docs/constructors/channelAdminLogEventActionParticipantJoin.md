---
title: channelAdminLogEventActionParticipantJoin
description: channelAdminLogEventActionParticipantJoin attributes, type and example
---
## Constructor: channelAdminLogEventActionParticipantJoin  
[Back to constructors index](index.md)






### Type: [ChannelAdminLogEventAction](../types/ChannelAdminLogEventAction.md)


### Example:

```
$channelAdminLogEventActionParticipantJoin = ['_' => 'channelAdminLogEventActionParticipantJoin', ];
```  

Or, if you're into Lua:  


```
channelAdminLogEventActionParticipantJoin={_='channelAdminLogEventActionParticipantJoin', }

```


