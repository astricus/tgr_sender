---
title: topPeerCategoryPhoneCalls
description: topPeerCategoryPhoneCalls attributes, type and example
---
## Constructor: topPeerCategoryPhoneCalls  
[Back to constructors index](index.md)






### Type: [TopPeerCategory](../types/TopPeerCategory.md)


### Example:

```
$topPeerCategoryPhoneCalls = ['_' => 'topPeerCategoryPhoneCalls', ];
```  

Or, if you're into Lua:  


```
topPeerCategoryPhoneCalls={_='topPeerCategoryPhoneCalls', }

```


