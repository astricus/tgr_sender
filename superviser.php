<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 24.10.17
 * Time: 15:09
 */

declare(strict_types=1);

ini_set('display_errors', '1');
error_reporting(E_ALL);

date_default_timezone_set('Europe/Moscow');

$output = [];
exec('ps -A -o command | grep \'tg_channels_parser/main.[p]hp\' ', $output, $ret);

if ($ret != 0) {
    // nothing found, restart
    // TODO: alarm
    exec('nohup php /var/www/tgr_sender/cron/tg_channels_parser/main.php &');
}
