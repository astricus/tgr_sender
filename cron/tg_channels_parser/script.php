<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 20.07.17
 * Time: 18:10
 */

declare(strict_types=1);

ini_set('display_errors', '1');
error_reporting(E_ALL);

date_default_timezone_set('Europe/Moscow');

require_once __DIR__ . '/../../../Default/lib/paths.php';
require_once DB_CLASS_LIB;
require_once TELEGRAM_MADELINE_LIB;

require_once __DIR__ . '/../../config_prod.php';

require_once __DIR__ . '/config.php';
require_once __DIR__ . '/functions.php';

$db = initDb();


    $db->atomic(
    'INSERT IGNORE INTO channels 
                        (`tg_name`, 
                         `tg_id`, 
                         `tg_title`, 
                         `name`, 
                         `need_join`, 
                         `need_leave`, 
                         `is_in_watch`, 
                         `id_discarded`, 
                         `status`,
                         `created`) 
            VALUES      (?, 
                         ?, 
                         ?, 
                         ?, 
                         ?, 
                         ?, 
                         ?, 
                         ?,
                         ?,
                         ?) ',
    "123",
    111111111,
    "123123123123",
    '',
    1,
    0,
    1,
    0,
    '',
    strtotime("now")
);