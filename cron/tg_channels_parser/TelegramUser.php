<?php

class TelegramUser
{
    private $_MadelineProto;
    private $_debug = true;

    /**
     * Create object from session file
     * https://daniil.it/MadelineProto/ // Storing sessions
     *
     * @param string $session_file session file
     *
     * @return TelegramUser
     */
    public static function fromSavedSession(string $session_file) : \TelegramUser
    {
        $mp = \danog\MadelineProto\Serialization::deserialize($session_file);
        return new self($mp);
    }

    /**
     * Save session to file.
     * https://daniil.it/MadelineProto/ // Storing sessions
     *
     * @param string $session_file filename to save session
     *
     * @return bool
     */
    public function saveSessionToFile(string $session_file) : bool
    {
        \danog\MadelineProto\Serialization::serialize(
            $session_file,
            $this->_MadelineProto
        );

        return true;
    }

    /**
     * TelegramUser constructor.
     *
     * @param \danog\MadelineProto\API $mp mp object
     */
    private function __construct(\danog\MadelineProto\API $mp)
    {
        $this->_MadelineProto = $mp;
    }

    /**
     * Login to telegram
     *
     * @param int    $api_id    api id of our app (my.telegram.org)
     * @param string $api_hash  api hash
     * @param string $phone_num phone number
     *
     * @return TelegramUser
     */
    public static function fromLogin(
        int $api_id,
        string $api_hash,
        string $phone_num
    ) : TelegramUser {
        $mp = new \danog\MadelineProto\API(
            [
                'app_info' =>
                    [
                        'api_id' => $api_id,
                        'api_hash' => $api_hash,
                    ],
            ]
        );

        $sentCode = $mp->phone_login($phone_num); // Send code
        echo 'Enter the code you received: ';
        $code = '';
        for ($x = 0; $x < $sentCode['type']['length']; $x++) {
            $code .= fgetc(STDIN);
        }
        // Complete authorization
        $authorization = $mp->complete_phone_login($code);

        if ($authorization['_'] === 'account.password') {
            $authorization = $mp->complete_2fa_login(
                readline(
                    'Please enter your password (hint ' . $authorization['hint'] . '): '
                )
            );
        }

        return new self($mp);
    }

    /**
     * Forward to other channel telegram
     *
     * @param int    $api_id    api id of our app (my.telegram.org)
     * @param string $api_hash  api hash
     * @param string $phone_num phone number
     *
     * @return TelegramUser
     */
    public function messageForward($from, $channel_id, $message_id)
    {
        echo "\nforward!\n\n";
        echo $from . " " . $channel_id .  ' ' . $message_id;

        try {
            $r = $this->_MadelineProto->messages->forwardMessages([ 'from_peer' => $from, 'to_peer' => $channel_id, 'id' => [$message_id]]);
            var_dump($r);
        } catch (Throwable $e) {

                echo "error " . $e->getMessage() . " message_id " . $message_id . " " .
                    ' from_peer ' . " " .  $from . " " .  ' to_peer ' . " " .  $channel_id;
        }


    }

    /**
     * Join tg channel
     *
     * @param string $channel_name channel
     *
     * @return array
     */
    public function joinChannel(string $channel_name) : array
    {
        try {
            $res = $this->_MadelineProto->channels->joinChannel(
                ['channel' => $channel_name]
            );
        } catch (Throwable $e) {
            if ($this->_debug) {
                echo "error while joining channel: " . $e->getMessage();
            }
            return ['is_error' => 1];
        }

        $upd = $res['chats'][0];

        $channel_id = $upd['id'];
        $channel_title = $upd['title'];
        //$channel_photo = $upd['photo'][''];

        /**
         * 'photo' =>
        array(3) {
        '_' =>
        string(9) "chatPhoto"
        'photo_small' =>
        array(5) {
        '_' =>
        string(12) "fileLocation"
        'dc_id' =>
        int(2)
        'volume_id' =>
        int(223021182)
        'local_id' =>
        int(684067)
        'secret' =>
        int(-6406187238198161167)
        }
        'photo_big' =>
        array(5) {
        '_' =>
        string(12) "fileLocation"
        'dc_id' =>
        int(2)
        'volume_id' =>
        int(223021182)
        'local_id' =>
        int(684069)
        'secret' =>
        int(-6932571617923102005)
        }
        }
         */

        return [
            'is_error' => 0,
            'id' => $channel_id,
            'title' => $channel_title
        ];
    }

    /**
     * Leave channel
     *
     * @param string $channel_name channel name
     *
     * @return bool
     */
    public function leaveChannel(string $channel_name) : bool
    {
        try {
            $this->_MadelineProto->channels->leaveChannel(
                ['channel' => $channel_name]
            );
        } catch (Throwable $e) {
            if ($this->_debug) {
                echo "error while leaving channel: " . $e->getMessage();
            }
            return false;
        }

        return true;
    }

    /**
     * Get channel participants
     *
     * @param string $channel_name channel name
     *
     * @return bool
     */
    public function getChannelParticipants(string $channel_name) : bool
    {
        try {
            $channelParticipants = $this->_MadelineProto->get_full_info($channel_name);
            var_dump($channelParticipants["full"]["participants_count"]);

        } catch (Throwable $e) {
            if ($this->_debug) {
                echo "error while getting channel participants: " . $e->getMessage();
            }
            return false;
        }

        try {
            $channelParticipants = $this->_MadelineProto->channels->getParticipants(
                ['channel' => $channel_name,
                    'filter' => 'channelParticipantsRecent', //  channelParticipantsRecent
                    'offset' => 0,
                    'limit' => 1
                    ]
            );
        } catch (Throwable $e) {
            if ($this->_debug) {
                echo "error while getting channel participants: " . $e->getMessage();
            }
            return false;
        }

        return $channelParticipants;
    }

    /**
     * Get channel info
     *
     * @param string $channel_name channel name
     *
     * @return bool
     */
    public function getChannelInfo(string $channel_id) : array
    {
        try {
            $channelInfo = $this->_MadelineProto->get_full_info($channel_id);
            //var_dump($channelInfo["Chat"]["username"]);

        } catch (Throwable $e) {
            if ($this->_debug) {
                echo "error while getting channel info: " . $e->getMessage();
            }
            return array();
        }
        return $channelInfo;
    }



    /**
     * Get updates
     *
     * @param int $offset  offset
     * @param int $timeout sleep timeout
     *
     * @return array
     */
    public function getUpdates(int $offset = 0, int $timeout = 5) : array
    {
        try {
            $updates = $this->_MadelineProto->API->get_updates(
                [
                    'offset' => $offset,
                    'limit' => 1000,
                    'timeout' => $timeout
                ]
            );
        } catch (Throwable $e) {
            if ($this->_debug) {
                // TODO: alarm
                echo "Error while get updates: " . $e->getMessage();
            }
            return [];
        }

        return $updates;
    }

    /**
     * Get messages from channel by ids
     *
     * @param string $channel      channel (InputChannel)
     * @param array  $messages_ids message ids, array of int
     *
     * @return array
     */
    public function getChannelMessages(string $channel, array $messages_ids) : array
    {
        try {
            $messages = $this->_MadelineProto->channels->getMessages(
                [
                    'channel' => $channel,
                    'id' => $messages_ids,
                ]
            );
        } catch (Throwable $e) {
            if ($this->_debug) {
                echo "Error while getting messages: " . $e->getMessage();
            }
            return [];
        }

        return $messages['messages'];
    }

}