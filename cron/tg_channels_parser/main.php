<?php

declare(strict_types=1);

ini_set('display_errors', '1');
error_reporting(E_ALL);

date_default_timezone_set('Europe/Moscow');

require_once __DIR__ . '/../../../Default/lib/paths.php';
require_once DB_CLASS_LIB;
require_once TELEGRAM_MADELINE_LIB;

require_once __DIR__ . '/../../config_prod.php';

require_once __DIR__ . '/config.php';
require_once __DIR__ . '/functions.php';

require_once __DIR__ . '/TelegramUser.php';

if (php_sapi_name() == 'cli') {
    if (isset($_SERVER['TERM'])) {
        $run_from = 'manual';
    } else {
        $run_from = 'cron';
    }
} else {
    $run_from = 'web';
}

if (!file_exists($session_file)) {
    if ($run_from != 'manual') {
        die("Login to telegram by running this script manually");
    }

    $tgUser = TelegramUser::fromLogin($api_id, $api_hash, $phone_num);
    $tgUser->saveSessionToFile($session_file);

    echo "Session saved. Now you can run script as daemon.\n";
    exit;
}

// TODO: double fork?
//Fork the current process
$processID = pcntl_fork();

//Check to make sure the forked ok.
if ($processID == -1) {
    echo "\n Error:  The process failed to fork. \n";
} elseif ($processID) {
    //This is the parent process.
    exit;
} else {
    //We're now in the child process.
    //Now, we detach from the terminal window, so that we stay alive when
    //it is closed.
    if (posix_setsid() == -1) {
        echo "\n Error: Unable to detach from the terminal window. \n";
    }
/*
    fclose(STDIN);
    fclose(STDOUT);
    fclose(STDERR);
    // https://stackoverflow.com/a/21053866
    $STDIN = fopen('/dev/null', 'r');
    $STDOUT = fopen('/dev/null', 'wb');
    $STDERR = fopen('/dev/null', 'wb');
*/

    //Get out process id now that we've detached from the window.
    $posixProcessID = posix_getpid();

    $tgUser = TelegramUser::fromSavedSession($session_file);

    register_shutdown_function(
        function (TelegramUser $tgUser, string $session_file) {
            $tgUser->saveSessionToFile($session_file);
        },
        $tgUser,
        $session_file
    );

    $db = initDb();

    $offset = 0;
    $sleep_timeout = 10;
    while (true) {
        // tasks with db
        // process telegram updates
        $updates = $tgUser->getUpdates($offset, $sleep_timeout);

        foreach ($updates as $update) {
            // Just like in the bot API, the offset must be set to the last update_id
            $offset = $update['update_id'] + 1;

            $update_type = $update['update']['_'];

            if ($update_type == 'updateNewChannelMessage') {
                echo "update init _________________________________________________________\n";
                $msg = $update['update']['message'];
                var_dump($msg);
                echo "update stopped _________________________________________________________\n";
                processTgChannelMessage($db, $msg, $tgUser);
            }
        }
        unset($updates);
        unset($update);
    }
}
