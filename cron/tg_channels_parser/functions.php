<?php
/**
 * Created by PhpStorm.
 * User: ivan
 *
 * PHP version 7.0
 */

ini_set('display_errors', 1);
error_reporting(E_ALL);

require_once __DIR__ . '/config.php';

date_default_timezone_set('Europe/Moscow');

function processTgChannelMessage(\DB $db, array $message, \TelegramUser $tgUser)
{
    var_dump($message);
    // сообщение в канал, а не оповещение о чем-то
    if ($message['_'] == 'message') {
        $msg_tg_id = $message['id'];
        $msg_ts = $message['date'];
        $msg_text = $message['message'];

        $ch_id = $message['to_id']['channel_id'];

        //проверка нет ли в БД уже сообщения с подобным id
        $check_message_exists = dbQuery(
            $db,
            'value',
            'select count(id)>0 from `ch_messages` WHERE tg_msg_id=? AND tg_channel_id=?',
            $msg_tg_id,
            $ch_id
        );

        if ($check_message_exists) {
            return;
        }

        if (array_key_exists('fwd_from', $message)) {
            // бывают форварды от людей
            if (array_key_exists('channel_id', $message['fwd_from'])) {
                $fwd_ch_id = $message['fwd_from']['channel_id'];
                $fwd_ts = $message['fwd_from']['date'];
                $fwd_channel_post = $message['fwd_from']['channel_post'];
            }
        }

        dbQuery(
            $db,
            'atomic',
            'INSERT IGNORE INTO ch_messages 
                        (`tg_channel_id`, 
                         `tg_msg_id`, 
                         `msg_timestamp`, 
                         `text`
                         ) 
            VALUES      (?, 
                         ?, 
                         ?, 
                         ?) ',
            $ch_id,
            $msg_tg_id,
            $msg_ts,
            $msg_text
        );


        //!!! репост

        /* проверить что канал для репоста */
        if($ch_id != CHANNEL_ID) {
            $tgUser->messageForward('channel#' . $ch_id, 'channel#' . CHANNEL_ID, $msg_tg_id);
        }

    }
}


/**
 * Try to make a query, on fail - reconnect ot database and try again.
 * TODO: fix possible infinite loop
 *
 * @param DB     $db      DB object
 * @param string $method  DB method
 * @param string $query   query to run
 * @param array  ...$args args for query
 *
 * @return mixed
 */
function dbQuery(\DB &$db, string $method, string $query, ...$args)
{
    try {
        $res = call_user_func([$db, $method], $query, ...$args);
    } catch (Throwable $e) {
        unset($db);
        sleep(2);
        $db = initDb();
        return dbQuery($db, $method, $query, ...$args);
    }

    return $res;
}

/**
 * Init DB
 *
 * @return DB
 */
function initDb() : \DB
{
    $db = new \DB(
        CONFIG_DB_HOST,
        CONFIG_DB_USER,
        CONFIG_DB_PASSWORD,
        //'tg_channels_parser'
        CONFIG_DB_TGR_NAME
    );

    return $db;
}