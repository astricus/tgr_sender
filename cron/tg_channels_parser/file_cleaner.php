<?php

ini_set("display_errors", 1);
error_reporting(E_ALL);

date_default_timezone_set('Europe/Moscow');

$path = "/var/www/tgr_parser/app/webroot/files/";

$files_delete_list = array();

$now = time();

function recurseRmdir($dir, $now) {
    $files = array_diff(scandir($dir), array('.','..'));
    foreach ($files as $file) {

        if(is_dir("$dir/$file")) {
            recurseRmdir("$dir/$file", $now);
        }
        else {
            if(file_exists($dir . "/" . $file)) {
                if ($now - filemtime($dir."/". $file) < 3600 * 24 * 7) continue;
                $files_delete_list[] = $dir ."/". $file;
            }
        }
        //было unlink("$dir/$file");
    }
    //return rmdir($dir);
}

recurseRmdir($path, $now);
print_r($files_delete_list);