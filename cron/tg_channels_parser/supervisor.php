<?php
/**
 * Created by PhpStorm.
 * User: ivan
 *
 * PHP Version 7.0
 */

declare(strict_types=1);

ini_set('display_errors', '1');
error_reporting(E_ALL);

date_default_timezone_set('Europe/Moscow');

$output = [];
exec('ps -A -o command | grep \'tg_channels_parser/main.[p]hp\' ', $output, $ret);

if ($ret != 0) {
    // nothing found, restart
    // TODO: alarm
    exec('nohup php ../tg_channels_parser/main.php &');
}
