-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Янв 30 2018 г., 18:48
-- Версия сервера: 5.7.19-17-log
-- Версия PHP: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `tgr_sender`
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `tgr_sender`
--

-- --------------------------------------------------------

--
-- Структура таблицы `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `add_date` datetime NOT NULL COMMENT 'дата регистрации',
  `login` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  `last_activity` datetime NOT NULL,
  `status` enum('active','blocked') NOT NULL COMMENT '1 - не активирован, 2 -  активирован, 3 - заблокирован, 4 - удален'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `channels`
--

CREATE TABLE `channels` (
  `tg_name` varchar(255) NOT NULL,
  `tg_id` varchar(255) NOT NULL,
  `tg_title` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `need_join` int(11) NOT NULL,
  `need_leave` int(11) NOT NULL,
  `is_in_watch` int(11) NOT NULL,
  `is_discarded` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `ch_messages`
--

CREATE TABLE `ch_messages` (
  `id` int(11) NOT NULL,
  `msg_timestamp` int(11) NOT NULL,
  `text` text NOT NULL,
  `tg_msg_id` bigint(20) NOT NULL,
  `tg_channel_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `ch_messages`
--

INSERT INTO `ch_messages` (`id`, `msg_timestamp`, `text`, `tg_msg_id`, `tg_channel_id`) VALUES
(13141, 1508497692, '55555', 42, '1088801533'),
(49503, 1517327161, 'http://telegra.ph/poll-373586-821769-01-30', 18834, '1140831835');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `channels`
--
ALTER TABLE `channels`
  ADD PRIMARY KEY (`tg_id`) USING BTREE,
  ADD UNIQUE KEY `tg_name` (`tg_name`) USING BTREE,
  ADD KEY `name` (`name`),
  ADD KEY `need_join` (`need_join`),
  ADD KEY `need_leave` (`need_leave`),
  ADD KEY `is_in_watch` (`is_in_watch`),
  ADD KEY `is_discarded` (`is_discarded`),
  ADD KEY `status` (`status`);

--
-- Индексы таблицы `ch_messages`
--
ALTER TABLE `ch_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tg_id` (`tg_msg_id`),
  ADD KEY `tg_channel_id` (`tg_channel_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `ch_messages`
--
ALTER TABLE `ch_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49504;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
